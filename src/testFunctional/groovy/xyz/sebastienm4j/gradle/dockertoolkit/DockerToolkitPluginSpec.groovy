package xyz.sebastienm4j.gradle.dockertoolkit

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class DockerToolkitPluginSpec extends Specification
{
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    File buildFile


    def setup()
    {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
            plugins {
                id 'xyz.sebastienm4j.gradle.docker-toolkit'
            }
        """
    }


    def "test"() {
        buildFile << """
            println 'test ok'
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        result.output.contains("test ok")
        result.task(":properties").outcome == SUCCESS
    }
}
