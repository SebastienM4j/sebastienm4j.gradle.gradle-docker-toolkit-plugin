Gradle Docker Toolkit Plugin
============================

[![Release](https://img.shields.io/badge/release-v0.1.0-blue.svg)](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/blob/master/changelog.md) [![GitLab pipeline status](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/badges/master/pipeline.svg)](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/commits/master) [![GitLab coverage report](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/badges/master/coverage.svg)](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/commits/master) [![License](https://img.shields.io/dub/l/vibe-d.svg)](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/blob/master/LICENSE)

Gradle plugin offering a toolkit to manage Docker images and containers.


License
-------

Gradle Docker Toolkit Plugin is released under [MIT license](https://gitlab.com/SebastienM4j/gradle-docker-toolkit-plugin/blob/master/LICENSE).
